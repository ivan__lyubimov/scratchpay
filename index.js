import Client from './src/Client.js'
import config from './config.js'

import { errReport, log } from './src/utils.js'

const api = new Client(config)
api.start(function (err, data) {
  if (err) return errReport.call(this, err)
  if (data) return log.call(this, data)
}.bind(process))                   