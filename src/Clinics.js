import moment from 'moment'

import dental from "../fixtures/dental.js";
import vets from "../fixtures/vets.js"
import States from "./States.js"
export default class Clinics {

  static facilities = [ ...normalize(dental), ...normalize(vets) ]

  static availabilityCheck(clinic, requestedAvailability) {
    const format = 'hh:mm'

    const opensAt = moment(clinic.availability.from, format)
    const closesAt = moment(clinic.availability.to, format)

    const visitStartsAt = moment(requestedAvailability.from, format)
    const visitEndsAt = moment(requestedAvailability.to, format)

    return (
      visitStartsAt.isBetween(opensAt, closesAt, 'minutes', []) && 
      visitEndsAt.isBetween(opensAt, closesAt, 'minutes', [])
    ) ? true : false
  }
  
  static async find({ name, state, availability } = {}) {
    return Clinics.facilities.filter((f) => (
          name && (f.name !== name) || 
          state && (f.state !== state) ||
          availability && (!Clinics.availabilityCheck(f, availability))
    ) ? false : true)
  }

  static async validateQuery(query) {
    const rules = commonRules(query)
    const checks = []
    const failedChecks = {}

    for (const prop in query) {
      checks.push(new Promise(async (resolve) => {
        if (await rules[prop].test()) return resolve()
        failedChecks[prop] = { warning: rules[prop].warning }
        resolve({ ...failedChecks[prop] })
      }))
    }
    
    await Promise.all(checks)
    if (Object.keys(failedChecks).length) return { valid: false, checks: failedChecks }
    return { valid: true }
  }
}

function commonRules(query) {
  return {
    availability: {
      warning: 'inalid prop: availability',
      test: () => new Promise((resolve) => {
        if (
          query['availability'] && query['availability'].from && query['availability'].to
        ) return resolve(true)
        resolve(false)
      })
    },
    state: {
      warning: 'invalid prop: state',
      test: () => new Promise((resolve) => {
        const stateCode = States.find(query['state'])
        if (query['state'] && stateCode) {
          query['state'] = stateCode
          return resolve(true)
        } 
        resolve(false)
      })
    },
    name: {
      warning: 'invalid prop: name',
      test: () => new Promise((resolve) => {
        if (query['name'] && query['name'].length) return resolve(true)
        resolve(false)
      })
    }
  }
}

function normalize(clinics) {
  return clinics.map((center) => {
    return {
      name: center.name || center.clinicName,
      state: States.find(center.stateCode || center.stateName),
      availability: center.availability || center.opening
    }
  })
}