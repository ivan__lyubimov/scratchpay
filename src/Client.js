import express from 'express'
import cors from 'cors'

import { en } from './utils.js'
import Clinics from './Clinics.js'

class Client {
  constructor(config) {
    this.config = config
  }

  async start(std) {
    const { err: ssErr, data: ssData } = await en(async () => await this.initServer())
    std(ssErr, ssData)
  }

  async queryHandler(query) {
    const { err: validationErr, data: validation } = await en(async () => await Clinics.validateQuery(query))
    if (validationErr || !validation.valid) return validationErr || new Error(JSON.stringify(validation.checks))

    const { err, data } = await en(async () => await Clinics.find(query))
    return err || data
  }

  app() {
    const app = express()

    app.use(cors())

    app.get('/clinics', async (req, res) => {
      try {
        const { err, data } = await en(async () => await Clinics.find())
        if (err) return res.status(400).send(err.message)
        return res.status(200).send(data)
      } catch (e) {
        return res.status(400).send(e.message)
      }
    })
    
    app.get('/clinics/:query', async (req, res) => {
      try {
        const query = JSON.parse(req.params.query || "{}")
        const { err, data } = await en(async () => await this.queryHandler(query))
        if (err) return res.status(400).send(err.message)
        return res.status(200).send(data)
      } catch (e) {
        return res.status(400).send(e.message)
      }
    })

    return app
  }

  initServer() {
    const { serverPort } = this.config
    this.app().listen(serverPort)
    return `server is running on port ${serverPort}`
  }
}

export default Client