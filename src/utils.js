import config from '../config.js'

export async function en(x) {
  if (isPrimitive(x)) return { err: 'passed primitive to en function' }

  // regular promise
  if (x.then) {
    return x.then((input = {}) => {
      const result = flatten(input)
      if (result instanceof Error) return { data: null, err: result }
      return { data: result, err: null }
    }).catch(err => {
      return { err }
    })
  }

  // async function
  if (x[Symbol.toStringTag] === 'AsyncFunction') {
    try {
      const input = await x() || {}
      const result = flatten(input)
      if (result instanceof Error) return { data: null, err: result }
      return { data: result, err: null }
    } catch(err) {
      return { err }
    }
  }

  // regular func
  if (typeof x === 'function') {
    return new Promise((resolve) => {
      try {
        const input = x() || {}
        const result = flatten(input)
        if (result instanceof Error) return resolve({ data: null, err: result })
        return resolve({ data: result, err: null })
      } catch (err) {
        return resolve({ err })
      }
    })
  }

  return { err: 'en function handler err' }
}

export function isPrimitive(test) {
  return test !== Object(test)
}

export function log(data) {
  console.log(getTimestamp(new Date), data)
}

export async function errReport(someErr) {
  const formattedErr = `
    ${getTimestamp(new Date)}
    New error at ${config.deviceName}, running ${config.moduleName}:
    ${someErr.stack}
  `
  const encoded = encodeURIComponent(formattedErr)
  console.log(formattedErr)

  if (!this.errors) this.errors = []
  if (this.errors.includes(JSON.stringify(someErr))) return
  this.errors.push(JSON.stringify(someErr))

  const { messageErr } = await en(networkMessage(encoded))
  if (messageErr) console.error(getTimestamp(new Date), formattedErr)
}


export async function networkMessage(text) {
  console.error(text)
}

export function isBoolean(val) {
  return 'boolean' === typeof val
}

function getTimestamp(date) {
  return `${date.toLocaleDateString()} | ${date.toLocaleTimeString('en-GB')}`
}

function flatten(input) {
  if (isPrimitive(input)) return input
  if (input.data) return flatten(input.data)
  if (input.err) return flatten(input.err)
  return input
}