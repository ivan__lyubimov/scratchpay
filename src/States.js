import stateCodes from '../fixtures/stateCodes.js'

export default class States {
  static codes = stateCodes.map(({ abbreviation, name }) => ({ 
    abbreviation: abbreviation.toLowerCase(), 
    name: name.toLowerCase()
  }))

  static find(nameOrCode) {
    const valid = States.codes.find(({ name, abbreviation }) => {
      if (
        nameOrCode.toLowerCase() === name ||
        nameOrCode.toLowerCase() === abbreviation
      ) return 1
    })

    return valid.abbreviation || false
  }
}