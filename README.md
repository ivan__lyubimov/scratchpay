This is a express based nodejs api repository for clinics search

Available endpoints:
```
/clinics – to get all records
/clinics/urlEncodedJsonQuery – to filter records by some criteria
```

Idea behind is to provide single flexible route that would support complex query notation 
Stringified object may include different arguments without significant changes neither in clients or backend side code.

All filters supported with any combination of states, state codes, availability and clinic names.

Additional request validation checks for correctness of provided query and responds with array of invalid arguments if there were any.

Example of request:
```
const filterBy = {
  "name": "Good Health Home", 
  "state": "florida", // "fl",
  "availability": {
    "from": "15:00",
    "to": "16:00"
  }
}

get(/clinics/${encodeURIComponent(JSON.stringify(filterBy))})
```

PS. Didn't finish in time with automated tests unfortunately
For convenience this online tool might be useful with query object encoding
```
https://onlinejsontools.com/url-encode-json

```

